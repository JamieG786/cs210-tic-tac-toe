# CS210 Tic Tac Toe

This is a basic program developed in C. It allows a user to play tic tac toe from the command line.
The user has the option to play against another user or an AI. The user then enteres the row and column they want to place their X or O on.

To run this game simply compile and run the ace2.c file from the command line.