/**************************************************************************
 * Assessment Title:
 *
 * Number of Submitted C Files: 
 *
 * Date: 18/10/2018
 *
 * 
 * Author: Jamie Greenaway, Reg no: 201719306
 *
 *
 * Personal Statement: I confirm that this submission is all my own work.
 *
 *          (Signed) Jamie Greenaway
 *
 * 
 *
 **************************************************************************/

/*This is a C program that will run a game of tic tac toe. This can be played against another user or the computer*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N 3
// declaring the functions that I will use with the parameters that they will pass
void user_prompt(char *message, char *answer);
void clear_board(char board[N][N]);
void display_board(char board[N][N]);
void user_move(char board[N][N]);
void computer_move(char board[N][N]);
int detect_win(char board[N][N]);
// global variable 
char XorO;


/* This is the main function. When the program is run this is the function that will run. This will begin by assigning variable message for the first message to be sent to the user. It will then call user_prompt to show the user the message and recieve an answer which will be stored in the appropriate variable back in the main function. It will do this once more before the program enters one of two ways dependent on if the user wants to play against another user or the computer. If they want to play against another user it will call the user_prompt another twice to get both players nicknames before entering a while loop which shall run the game. This shall run the game until a user has one or the game has ended in a draw. The program will finish by telling the user who won or if the game was a draw. If they selected that they wanted to play the computer it will only call user_prompt once to get their name. It will then enter a very similar while loop to run the game against the computer until someone has won or the game is a draw. The program will then finish by telling the user who won or if the game ended in a draw. */
int main(void) {
	char board[N][N];
	char message[500];
	char answer[10];
	char UorC;
	char nickname[50];
	char nickname2[50];
	char XorOother;
	
	strcpy(message, "Would you like to play against another user or computer? Enter U for user or C for computer \n");
	user_prompt(message, answer);
	while (answer[0] != 'U' || answer[0] != 'C') {
		if (answer[0] == 'U' || answer[0] == 'C') {
			break;
		}
		strcpy(message, "That was not a valid input. Please try again \n");
		user_prompt(message, answer);
		
	}
	UorC = answer[0];

	strcpy(message, "Would you like to play using X or O? \n");
	user_prompt(message, answer);
	while (answer[0] != 'X' || answer[0] != 'O') {
		if (answer[0] == 'X' || answer[0] == 'O') {
			break;
		}
		strcpy(message, "That was not a valid input. Please try again \n");
		user_prompt(message, answer);
		

	}
	XorO = answer[0];

	if (UorC == 'C') {
		strcpy(message, "Please enter a nickname \n");
		user_prompt(message, answer);
		strcpy(nickname, answer);

		clear_board(board);
		display_board(board);

		while(detect_win(board) == 0) {
			user_move(board);
			if (detect_win(board) == 0) {
				computer_move(board);
				detect_win(board);
			}
			if (detect_win(board) == 1) {
				printf("X has won the game \n"); 
				break;
			} else if (detect_win(board) == 2) {
				printf("O has won the game \n");
				break;
			} else if (detect_win(board) == 3) {
				printf("The game has ended in a draw \n");
				break;
			}
		}
	}

	if (UorC == 'U') {
		strcpy(message, "Please enter a nickname for user 1 \n");
		user_prompt(message, answer);
		strcpy(nickname, answer);			

		strcpy(message, "Please enter a nickname for user 2 \n");
		user_prompt(message, answer);
		strcpy(nickname2, answer);
		
		if (XorO == 'X') {
			XorOother = 'O';
		} else if (XorOother == 'O') {
			XorOother = 'X';
		}

		printf("%s will play with character %c and %s will play with character %c \n", nickname, XorO, nickname2, XorOother);	
	
		clear_board(board);
		display_board(board);

		while(detect_win(board) == 0) {
			user_move(board);
			if (detect_win(board) == 1) {
				printf("X has won the game \n"); 
				break;
			} else if (detect_win(board) == 2) {
				printf("O has won the game \n");
				break;
			} else if (detect_win(board) == 3) {
				printf("The game has ended in a draw \n");
				break;
			}
			
		}
	}	

	return 0;
}

/* This is the function that recieves user input from the starting messages. It gets the message passed in and then passes the answer out.*/ 
void user_prompt(char *message, char *answer) {

	printf("%s", message);
	scanf("%s", answer);
}

/* This function will clear the game board. Using two for loops, it will run through the 3x3 array and in every space place '-' to symbolise an empty slot in the board. */
void clear_board(char board[N][N]) {
	int i = 0;
	int j = 0;
	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			board[i][j] = '-';
		}
	}
}

/* This function will display the board. Using two for loops, it will run through the 3x3 array element by element and print off an element at every stage until the fill board is displayed. */
void display_board(char board[N][N]) {
	int i = 0;
	int j = 0;
	printf("Here is the board: \n");
	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			printf("%c", board[i][j]);
		}
	printf("\n");
	}
}

/* This is the function that will make a user move. It starts by prompting the user to enter a value for the column they would like to place their character in. It will then run an if statement to check that input is valid and if it is not, display an error message. It will then prompt the user to enter a value for the row they would like to place their character in. It will run another if statement to make sure that input is valid. It will then run a validation to make sure that there isnt already another character where the user wants to place theirs. If there is an error message will be displayed and they will be prompted to enter their values again. If there isnt it will enter that character onto that place and then call the display_board function. */
void user_move(char board[N][N]) {
	int row;
	int column;
	printf("Please choose a column to place your character in \n");
	scanf("%d", &column);

	while (column < 0 || column > 3) {	
		if (column > 0 && column < 3) {
			break;
		}
		printf("That input is out of bounds. Please enter a column between 1 and 3 \n");
		scanf("%d", &column);
	}	
	
	printf("Please choose a row to place your character in \n");
	scanf("%d", &row);


	while (row < 0 || row > 3) {	
		if (row > 0 && row < 3) {
			break;
		}
		printf("That input is out of bounds. Please enter a column between 1 and 3 \n");
		scanf("%d", &column);
	}
	
	while (board[row-1][column-1] != '-') {
		if (board[row-1][column-1] == '-') {
			break;
		}
		printf("There is already a character on that spot. Please try again \n");

		printf("Please choose a column to place your character in \n");
		scanf("%d", &column);

	while (column < 0 || column > 3) {	
		if (column > 0 && column < 3) {
			break;
		}
		printf("That input is out of bounds. Please enter a column between 1 and 3 \n");
		scanf("%d", &column);
		}	
		
		printf("Please choose a row to place your character in \n");
		scanf("%d", &row);

		while (row < 0 || row > 3) {	
			if (row > 0 && row < 3) {
				break;
			}
			printf("That input is out of bounds. Please enter a column between 1 and 3 \n");
			scanf("%d", &column);
		}
	}
	board[row-1][column-1] = XorO;
	display_board(board);	

	if (XorO == 'X') {
		XorO = 'O';
	} else if (XorO == 'O') {
		XorO = 'X';
	}	

}

/* This is the function that will simulate a computer move. It uses two random number generators to get the column and the row for where the character will be placed. It then uses a validation to make sure there isnt already a character in this space and if there is, it will generate another two random numbers until it gets a area in the array that will be accepted. */
void computer_move(char board[N][N]) {

	int row = rand() % 3 + 1;
	int column = rand() % 3 + 1;

	while(board[row-1][column-1] !='-') {
		if (board[row-1][column-1] == '-') {
			break;
		}
		row = rand() % 3 + 1;
		column = rand() % 3 + 1;
	}	
	board[row-1][column-1] = XorO;
	display_board(board);	

	if (XorO == 'X') {
		XorO = 'O';
	} else if (XorO == 'O') {
		XorO = 'X';
	}	
}

/* This is the function that will detect if someone has won or the user has drawn. It starts by checking for horizontal then vertical, diagonally and finally it checks for a draw. It checks for a horizontal and vertical wins by using two for loops. After one of the for loops is assigns a 2d array meaning it will only go horizontally or vertically. It then uses two if statements to check for a win for X and then a win for O. For diagonal win it just uses four if statements to go through the possible wins for diagonal. It does this for X and O for both diagonals hence the 4 if statements. It then finishes the function by counting how many '-' are in the table. If the count is equal to zero, there is no more empty space in the board therefore the game is a draw. The function then returns the value won. If won = 1, X has won, x = 2 means O has won and x = 3 means the game has ended in a draw. */
int detect_win(char board[N][N]) {

	int x;
	int y;
	char array[3];
	int won =0;

	// horizontal
	for (x=0; x<3; x++) {
		array[0] = ' ';
		array[1] = ' ';
		array[2] = ' ';
		for (y=0; y<3; y++) {
			array[y] = board[x][y];

			if (array[0] == 'X' && array[1] == 'X' && array[2] == 'X') {
				won = 1;
				break;
			} else if (array[0] == 'O' && array[1] == 'O' && array[2] == 'O') {
				won = 2;
				break;
			}

		}
	} 
	array[0] = ' ';
	array[1] = ' ';
	array[2] = ' ';

	// vertical
	for (y=0; y<3; y++) {
		array[0] = ' ';
		array[1] = ' ';
		array[2] = ' ';
		for (x=0; x<3; x++) {
			array[x] = board[x][y];

			if (array[0] == 'X' && array[1] == 'X' && array[2] == 'X') {
				won = 1;
				break;
			} else if (array[0] == 'O' && array[1] == 'O' && array[2] == 'O') {
				won = 2;
				break;
			}

		}
	}
	array[0] = ' ';
	array[1] = ' ';
	array[2] = ' ';

	// diagonal 
	if (board[0][0] == 'X' && board[1][1] == 'X' && board[2][2] == 'X') {
		won = 1;
	} else if (board[0][0] == 'O' && board[1][1] == 'O' && board[2][2] == 'O') {
		won = 2;
	}
	
	if (board[0][2] == 'X' && board[1][1] == 'X' && board[2][0] == 'X') {
		won = 1;
	} else if (board[0][2] == 'O' && board[1][1] == 'O' && board[2][0] == 'O') {
		won = 2;
	} 

	//draw
	int count = 0;
		for (y=0; y<3; y++) {
			for (x=0; x<3; x++) {
				if (board[x][y] == '-') {
					count++;
				}
			}
		}
	
	if (count == 0) {
		won = 3;
	}
	return won;
}


